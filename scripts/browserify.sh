#!/bin/bash

# <!> this script needs to have browserify installed.
# 	to do so :
# 	npm install -g browserify
# 	(you may need to be root)

mkdir -p bin
find dist/src/ -name "*.js" | xargs browserify > bin/pan-iframe.js
cp node_modules/pan-app-manager/bin/pan-app-manager.js bin/pan-app-manager.js

rm HTML/pan-app-manager.js 2> /dev/null
rm HTML/pan-iframe.js 2> /dev/null
ln -s $(pwd)/bin/pan-app-manager.js HTML/
ln -s $(pwd)/bin/pan-iframe.js HTML/

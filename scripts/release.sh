#!/bin/bash

# <!> this script needs to have minify installed.
# 	to do so :
# 	npm install -g minify
# 	(you may need to be root)

mkdir -p bin
uglify -s bin/pan-iframe.js -o bin/pan-iframe.min.js
mv bin/pan-iframe.min.js bin/pan-iframe.js
cp node_modules/pan-app-manager/bin/pan-app-manager.min.js bin/pan-app-manager.js

rm HTML/pan-app-manager.js 2> /dev/null
rm HTML/pan-iframe.js 2> /dev/null
ln -s $(pwd)/bin/pan-app-manager.js HTML/
ln -s $(pwd)/bin/pan-iframe.js HTML/

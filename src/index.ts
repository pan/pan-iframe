import { Duplex } from 'stream'
import { Message, messageType } from 'pan-app-manager'
import { ErrorMessage, forgeErrorMessage } from 'pan-app-manager'
import { RequestStreamMessage } from 'pan-app-manager'
var PostMessageStream = require('post-message-stream')

let iframe: HTMLIFrameElement


let parseRequestStreamMessage: (message: RequestStreamMessage) => void = (message: RequestStreamMessage): void =>
{
  if (message.isEntry())
  {
    let stream: Duplex = new PostMessageStream(
    {
        name: 'iframe-parent-pan-file-entry',
        target: 'iframe-child-pan-file-entry',
        targetWindow: iframe.contentWindow,
    })

    stream.write('<h1>Hello word!</h1>')
    setTimeout(function ()
    {
      stream.destroy()
    }, 1)

  }
}

window.onload = function ()
{

  let xhr: XMLHttpRequest = new XMLHttpRequest()
  xhr.open('GET', 'pan-app-manager.js', true)
  xhr.responseType = 'text'
  xhr.onload = function(e)
  {
    if (this.status == 200)
    {
      let blob: Blob = new Blob(['<script>' + this.response +  '<\/script>'], {type: 'text/html'})
      let url: string = URL.createObjectURL(blob)
      iframe = document.createElement('iframe')
      iframe.src = url
      iframe.id = 'iframe'
      iframe.setAttribute('sandbox', 'allow-scripts')

      document.body.appendChild(iframe)


      let stream: Duplex = new PostMessageStream(
      {
          name: 'iframe-parent-pan-main',
          target: 'iframe-child-pan-main',
          targetWindow: iframe.contentWindow,
      })

      stream.on('data', function (chunk: any)
      {
        let message: Message | null = Message.parse(chunk)
        if (message)
        {
          switch (message.getType())
          {
            case messageType.ERROR:
              console.log("[COMM-parrent] error message received: " + (message as ErrorMessage).getErrorMessage())
            break
            case messageType.REQUEST_STREAM:
              console.log('[COMM-parrent] request stream message received: "' + (message as RequestStreamMessage).getFileName() + '"')
              parseRequestStreamMessage(message as RequestStreamMessage)
            break
            default:
          }
        }
      })

    }
  }
  xhr.send();

}
